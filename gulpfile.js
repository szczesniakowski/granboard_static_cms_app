// gulp watch --option 999


const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');


gulp.task('build', () => {
    return gulp.src(['dev/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('public/style/'));

});

gulp.task('watch', () => {
    gulp.watch(
        ['dev/*.scss'],
        gulp.series('build')
    );
});